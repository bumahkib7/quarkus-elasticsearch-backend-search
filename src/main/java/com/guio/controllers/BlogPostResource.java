package com.guio.controllers;

import com.guio.dto.BlogPostDTO;
import com.guio.dto.FeedbackmessageDTO;
import com.guio.models.BlogPost;
import com.guio.service.BlogPostCacheService;
import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.hibernate.search.mapper.orm.session.SearchSession;
import org.jboss.resteasy.reactive.RestQuery;

import java.util.List;
import java.util.Optional;

import static io.quarkus.hibernate.orm.panache.PanacheEntityBase.listAll;

@Path("/blogposts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BlogPostResource {

    @Inject
    SearchSession searchSession;

    @Inject
    BlogPostCacheService cacheService;

    @PostConstruct
    @Transactional
    public void initialize() throws InterruptedException {
        if (BlogPost.count() > 0) {
            searchSession.massIndexer().startAndWait();
            List<BlogPost> allBlogPosts = BlogPost.listAll();
            cacheService.preloadCache(allBlogPosts);
        }
    }

    @GET
    @Path("/search")
    @Transactional
    public Response searchBlogPosts(@RestQuery String pattern, @RestQuery Optional<Integer> size) {
        String cacheKey = "search:" + pattern + ":" + size.orElse(20);
        BlogPostCacheService.CacheResponse cacheResponse = cacheService.getCachedBlogPosts(cacheKey);
        List<BlogPost> blogPosts = cacheResponse.blogPosts();
        boolean fromCache = cacheResponse.fromCache();

        if (blogPosts.isEmpty() && !fromCache) {
            blogPosts = searchSession.search(BlogPost.class)
                    .where(f -> pattern == null || pattern.trim().isEmpty() ?
                            f.matchAll() :
                            f.bool().with(b -> {
                                String wildcardPattern = "*" + pattern.toLowerCase() + "*";
                                b.should(f.wildcard().field("title").matching(wildcardPattern));
                                b.should(f.wildcard().field("body").matching(wildcardPattern));
                                b.should(f.wildcard().field("description").matching(wildcardPattern));
                            }))
                    .sort(f -> f.field("title_sort").then().field("description_sort"))
                    .fetchHits(size.orElse(20));
            cacheService.cacheBlogPosts(cacheKey, blogPosts);
        }

        List<BlogPostDTO> blogPostDTOs = blogPosts.stream().map(this::toDTO).toList();
        return Response.ok(blogPostDTOs)
                .header("X-Cache-Hit", fromCache)
                .header("X-Cache-Hits", cacheResponse.cacheHits())
                .header("X-Cache-Misses", cacheResponse.cacheMisses())
                .build();
    }

    @GET
    @Path("/all")
    @Transactional
    public Response getAllBlogPosts() {
        String cacheKey = "allBlogPosts";
        BlogPostCacheService.CacheResponse cacheResponse = cacheService.getCachedBlogPosts(cacheKey);
        List<BlogPost> posts = cacheResponse.blogPosts();
        boolean fromCache = cacheResponse.fromCache();

        if (posts.isEmpty() && !fromCache) {
            posts = listAll();
            cacheService.cacheBlogPosts(cacheKey, posts);
        }

        List<BlogPostDTO> dtoPosts = posts.stream().map(this::toDTO).toList();
        return Response.ok(dtoPosts)
                .header("X-Cache-Hit", fromCache)
                .header("X-Cache-Hits", cacheResponse.cacheHits())
                .header("X-Cache-Misses", cacheResponse.cacheMisses())
                .build();
    }

    @GET
    @Path("/{id}")
    public Response getBlogPostById(@PathParam("id") Long id) {
        String cacheKey = "blogPost:" + id;
        BlogPostCacheService.CacheResponse cacheResponse = cacheService.getCachedBlogPosts(cacheKey);
        List<BlogPost> blogPosts = cacheResponse.blogPosts();
        BlogPost blogPost;
        boolean fromCache = cacheResponse.fromCache();

        if (blogPosts.isEmpty()) {
            blogPost = BlogPost.findById(id);
            if (blogPost != null) {
                cacheService.cacheBlogPosts(cacheKey, List.of(blogPost));
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } else {
            blogPost = blogPosts.get(0);
        }

        return Response.ok(toDTO(blogPost))
                .header("X-Cache-Hit", fromCache)
                .header("X-Cache-Hits", cacheResponse.cacheHits())
                .header("X-Cache-Misses", cacheResponse.cacheMisses())
                .build();
    }

    @GET
    @Path("/search/title")
    @Transactional
    public Response searchBlogPostsByTitle(@RestQuery String title, @RestQuery Optional<String> sizeStr) {
        int size = sizeStr.map(s -> {
            try {
                return Integer.parseInt(s.trim());
            } catch (NumberFormatException e) {
                return 20; // default size if parsing fails
            }
        }).orElse(20);
        String cacheKey = "search:title:" + title + ":" + size;
        BlogPostCacheService.CacheResponse cacheResponse = cacheService.getCachedBlogPosts(cacheKey);
        List<BlogPost> blogPosts = cacheResponse.blogPosts();
        boolean fromCache = cacheResponse.fromCache();

        if (blogPosts.isEmpty() && !fromCache) {
            blogPosts = searchSession.search(BlogPost.class)
                    .where(f -> title == null || title.trim().isEmpty() ?
                            f.matchAll() :
                            f.simpleQueryString()
                                    .fields("title")
                                    .matching(title))
                    .sort(f -> f.field("title_sort"))
                    .fetchHits(size);
            cacheService.cacheBlogPosts(cacheKey, blogPosts);
        }

        List<BlogPostDTO> blogPostDTOs = blogPosts.stream().map(this::toDTO).toList();
        return Response.ok(blogPostDTOs)
                .header("X-Cache-Hit", fromCache)
                .header("X-Cache-Hits", cacheResponse.cacheHits())
                .header("X-Cache-Misses", cacheResponse.cacheMisses())
                .build();
    }

    @GET
    @Path("/search/description")
    @Transactional
    public Response searchBlogPostsByDescription(@RestQuery String description, @RestQuery Optional<String> sizeStr) {
        int size = sizeStr.map(s -> {
            try {
                return Integer.parseInt(s.trim());
            } catch (NumberFormatException e) {
                return 20; // default size if parsing fails
            }
        }).orElse(20);
        String cacheKey = "search:description:" + description + ":" + size;
        BlogPostCacheService.CacheResponse cacheResponse = cacheService.getCachedBlogPosts(cacheKey);
        List<BlogPost> blogPosts = cacheResponse.blogPosts();
        boolean fromCache = cacheResponse.fromCache();

        if (blogPosts.isEmpty() && !fromCache) {
            blogPosts = searchSession.search(BlogPost.class)
                    .where(f -> description == null || description.trim().isEmpty() ?
                            f.matchAll() :
                            f.simpleQueryString()
                                    .fields("description")
                                    .matching(description))
                    .sort(f -> f.field("description_sort"))
                    .fetchHits(size);
            cacheService.cacheBlogPosts(cacheKey, blogPosts);
        }

        List<BlogPostDTO> blogPostDTOs = blogPosts.stream().map(this::toDTO).toList();
        return Response.ok(blogPostDTOs)
                .header("X-Cache-Hit", fromCache)
                .header("X-Cache-Hits", cacheResponse.cacheHits())
                .header("X-Cache-Misses", cacheResponse.cacheMisses())
                .build();
    }

    private BlogPostDTO toDTO(BlogPost blogPost) {
        FeedbackmessageDTO feedbackDTO = blogPost.getFeedback() != null
                ? new FeedbackmessageDTO(blogPost.getFeedback().getId(), blogPost.getFeedback().getMessage())
                : null;
        return new BlogPostDTO(
                blogPost.getId(),
                blogPost.getTitle(),
                blogPost.getBody(),
                blogPost.getDescription(),
                blogPost.getSlug(),
                blogPost.getStatus(),
                feedbackDTO
        );
    }
}
