package com.guio.models;

import com.guio.abstractClasses.AbstractEntity;
import com.guio.models.Blogs.Feedbackmessage;
import com.guio.models.Blogs.GradeClassgrade;
import com.guio.models.user.User;
import io.smallrye.common.constraint.NotNull;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.search.mapper.pojo.automaticindexing.ReindexOnUpdate;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.*;

import static org.hibernate.search.engine.backend.types.Sortable.YES;

@Entity
@Table(name = "blog_post", schema = "public", indexes = {
        @Index(name = "blog_post_feedback_id_980ddc0a", columnList = "feedback_id"),
        @Index(name = "blog_post_slug_b95473f2_like", columnList = "slug"),
        @Index(name = "blog_post_grade_level_id_2f12c673", columnList = "grade_level_id"),
        @Index(name = "blog_post_owner_id_ff7c9277", columnList = "owner_id")
}, uniqueConstraints = {
        @UniqueConstraint(name = "blog_post_slug_b95473f2_uniq", columnNames = {"slug"})
})
@Indexed(index = "blog_post")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class BlogPost extends AbstractEntity {

    @NotNull
    @Column(name = "title", nullable = false, length = 100)
    @FullTextField(analyzer = "title_analyzer")
    @KeywordField(name = "title_sort", sortable = YES, normalizer = "sort")
    private String title;

    @NotNull
    @Column(name = "body", nullable = false)
    @FullTextField(analyzer = "body_analyzer")
    private String body;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_id")
    @ToString.Exclude
    @IndexedEmbedded
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    private User owner;

    @Column(name = "description", length = 400)
    @FullTextField
    @KeywordField(name = "description_sort", normalizer = "sort", sortable = YES)
    private String description;

    @NotNull
    @Column(name = "slug", nullable = false, length = 100)
    @KeywordField
    private String slug;

    @Column(name = "image", length = 900)
    private String image;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "grade_level_id")
    @ToString.Exclude
    @IndexedEmbedded
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    private GradeClassgrade gradeLevel;

    @NotNull
    @Column(name = "status", nullable = false, length = 100)
    private String status;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "feedback_id")
    @ToString.Exclude
    @IndexedEmbedded
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    private Feedbackmessage feedback;
}
