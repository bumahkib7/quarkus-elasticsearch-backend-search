package com.guio.abstractClasses;

import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;

import java.time.ZonedDateTime;

public class AuditingEntityListener {

    @PrePersist
    void onPrePersist(AbstractEntity entity) {

        ZonedDateTime now = ZonedDateTime.now();
        entity.setCreated(now);
        entity.setModified(now);
        entity.setDeletedAt(now);
    }

    @PreUpdate
    void onPreUpdate(AbstractEntity entity) {
        entity.setModified(ZonedDateTime.now());
    }
}