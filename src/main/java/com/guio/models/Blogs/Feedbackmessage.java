package com.guio.models.Blogs;

import com.guio.abstractClasses.AbstractEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.KeywordField;

@Entity
@Table(name = "blog_feedbackmessage", schema = "public")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Indexed
@EqualsAndHashCode(callSuper = true)
public class Feedbackmessage extends AbstractEntity {

    @Column(name = "message", length = 700)
    @KeywordField(name = "message_sort", normalizer = "sort")
    private String message;

}