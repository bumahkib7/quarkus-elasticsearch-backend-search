package com.guio.models.Blogs;

import com.guio.abstractClasses.AbstractEntity;
import io.smallrye.common.constraint.NotNull;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;

import java.util.Objects;


@Entity
@Table(name = "grade_classgrade")
@Indexed
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class GradeClassgrade extends AbstractEntity {


    @Column(name = "grade", length = 150)
    private String grade;

    @NotNull
    @Column(name = "country", nullable = false, length = 2)
    private String country;

    @Column(name = "color_code", length = 18)
    private String colorCode;

    @NotNull
    @Column(name = "ranking", nullable = false)
    private Integer ranking;

    @Override
    public final boolean equals(Object o) {
        if (this == o) return true;
        if (o == null) return false;
        Class<?> oEffectiveClass = o instanceof HibernateProxy ? ((HibernateProxy) o).getHibernateLazyInitializer().getPersistentClass() : o.getClass();
        Class<?> thisEffectiveClass = this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass() : this.getClass();
        if (thisEffectiveClass != oEffectiveClass) return false;
        GradeClassgrade that = (GradeClassgrade) o;
        return getId() != null && Objects.equals(getId(), that.getId());
    }

    @Override
    public final int hashCode() {
        return this instanceof HibernateProxy ? ((HibernateProxy) this).getHibernateLazyInitializer().getPersistentClass().hashCode() : getClass().hashCode();
    }
}