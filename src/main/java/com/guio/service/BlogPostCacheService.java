package com.guio.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.guio.models.BlogPost;
import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.value.ValueCommands;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Singleton
public class BlogPostCacheService {

    private final ValueCommands<String, String> valueCommands;
    private final ObjectMapper objectMapper;
    private final AtomicInteger cacheHits = new AtomicInteger();
    private final AtomicInteger cacheMisses = new AtomicInteger();

    @Inject
    public BlogPostCacheService(RedisDataSource redisDataSource, ObjectMapper objectMapper) {
        this.valueCommands = redisDataSource.value(String.class, String.class);
        this.objectMapper = objectMapper;
    }

    public record CacheResponse(List<BlogPost> blogPosts, boolean fromCache, int cacheHits, int cacheMisses) {}

    public CacheResponse getCachedBlogPosts(String key) {
        String cachedValue = valueCommands.get(key);
        if (cachedValue != null) {
            cacheHits.incrementAndGet();
            return new CacheResponse(deserialize(cachedValue), true, cacheHits.get(), cacheMisses.get());
        }
        cacheMisses.incrementAndGet();
        return new CacheResponse(Collections.emptyList(), false, cacheHits.get(), cacheMisses.get());
    }

    public void cacheBlogPosts(String key, List<BlogPost> blogPosts) {
        valueCommands.set(key, serialize(blogPosts));
    }

    public void preloadCache(List<BlogPost> blogPosts) {
        cacheBlogPosts("allBlogPosts", blogPosts);
        blogPosts.forEach(blogPost -> cacheBlogPosts("blogPost:" + blogPost.getId(), List.of(blogPost)));
    }

    private String serialize(List<BlogPost> blogPosts) {
        try {
            return objectMapper.writeValueAsString(blogPosts);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Error serializing blog posts", e);
        }
    }

    private List<BlogPost> deserialize(String serialized) {
        try {
            return objectMapper.readValue(serialized,
                    objectMapper.getTypeFactory().constructCollectionType(List.class, BlogPost.class));
        } catch (IOException e) {
            throw new RuntimeException("Error deserializing blog posts", e);
        }
    }
}
