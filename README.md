# QuarkusSearchMaster

Welcome to **QuarkusSearchMaster** – a high-performance, scalable platform for full-text search and efficient caching,
powered by Quarkus, Hibernate Search, and Redis.
This project showcases seamless integration of search capabilities with robust caching mechanisms,
delivering a snappy and responsive user experience.

## Features

- **Full-Text Search:** Leverage Hibernate Search for powerful search capabilities across various entities.
- **Efficient Caching:** Utilize Redis to cache search results and frequently accessed data to minimize database load and reduce response times.
- **RESTful API:** Clean and intuitive API endpoints for managing and querying data.
- **Automatic Indexing:** Automatically indexes existing entities on startup to ensure search efficiency.

## Table of Contents

- [Getting Started](#getting-started)
- [Configuration](#configuration)
- [API Endpoints](#api-endpoints)
- [Preloading Cache](#preloading-cache)
- [Running the Application](#running-the-application)
- [Technologies Used](#technologies-used)
- [License](#license)
- [Acknowledgements](#acknowledgements)

## Getting Started

### Prerequisites

- Java 20
- Docker
- Maven

### Clone the Repository

```bash
git clone https://github.com/your-username/QuarkusSearchMaster.git
cd QuarkusSearchMaster
```

## Configuration
Set up your application.properties file with the following configurations:
```properties
quarkus.datasource.db-kind=postgresql
quarkus.datasource.username=${DB_USERNAME}
quarkus.datasource.password=${DB_PASSWORD}
quarkus.datasource.jdbc.url=${DB_URL}

quarkus.hibernate-orm.dialect=org.hibernate.dialect.PostgreSQLDialect
quarkus.hibernate-orm.database.generation=update

quarkus.log.category."org.hibernate.SQL".level=DEBUG
quarkus.log.category."org.hibernate.type.descriptor.sql.BasicBinder".level=TRACE

quarkus.hibernate-search-orm.elasticsearch.version=8
quarkus.hibernate-search-orm.elasticsearch.analysis.configurer=com.guio.searchers.AnalysisConfigurer
quarkus.hibernate-search-orm.indexing.plan.synchronization.strategy=sync

quarkus.ssl.native=false

quarkus.redis.hosts=redis://localhost:6379
quarkus.cache.enabled=true
```

## Docker Compose
To run PostgreSQL and Redis using Docker, use the provided docker-compose.yml file:
```yaml
version: '3.8'
services:
  postgres:
    image: postgres:latest
    environment:
      POSTGRES_USER: ${DB_USERNAME}
      POSTGRES_PASSWORD: ${DB_PASSWORD}
      POSTGRES_DB: quarkus_blog
    ports:
      - "5432:5432"

  redis:
    image: redis:latest
    ports:
      - "6379:6379"
```
Start the services:
```bash
docker-compose up
```

## API Endpoints

### Search Blog Posts

- **Endpoint:** `/blogposts/search`
- **Method:** `GET`
- **Description:** Search for blog posts by a pattern.
- **Parameters:**
  - `pattern`: The search pattern.
  - `size`: Number of results to return (optional).

### Get All Blog Posts

- **Endpoint:** `/blogposts/all`
- **Method:** `GET`
- **Description:** Retrieve all blog posts.

### Get Blog Post by ID

- **Endpoint:** `/blogposts/{id}`
- **Method:** `GET`
- **Description:** Retrieve a blog post by its ID.

### Search Blog Posts by Title

- **Endpoint:** `/blogposts/search/title`
- **Method:** `GET`
- **Description:** Search for blog posts by title.
- **Parameters:**
  - `title`: The title of the blog post.
  - `size`: Number of results to return (optional).

### Preloading Cache

During startup, the application will index existing blog posts and preload them into the cache.
This ensures the cache is ready for immediate use, enhancing performance and reducing load times.

## Running the Application

Compile and run the application using Maven:

```bash
./mvnw compile quarkus:dev
```

## Technologies Used

- **Quarkus:** Supersonic Subatomic Java framework.
- **Hibernate Search:** Full-text search capabilities.
- **PostgreSQL:** Relational database.
- **Redis:** In-memory data structure store, used as a cache.
- **RESTEasy Reactive:** To build RESTful web services.

## License

This project is licensed under the MIT License—see the LICENSE file for details.

## Acknowledgements

Special thanks to the contributors and the open-source community for their support and contributions.
