package com.guio.models.Blogs;

import com.guio.abstractClasses.AbstractEntity;
import com.guio.models.BlogPost;
import com.guio.models.user.User;
import io.smallrye.common.constraint.NotNull;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.search.mapper.pojo.automaticindexing.ReindexOnUpdate;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.FullTextField;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.Indexed;
import org.hibernate.search.mapper.pojo.mapping.definition.annotation.IndexingDependency;

@Entity
@Table(name = "blog_comment", schema = "public", indexes = {
        @Index(name = "blog_comment_post_id_580e96ef", columnList = "post_id"),
        @Index(name = "blog_comment_owner_id_d904784e", columnList = "owner_id")
})
@Getter
@Setter
@ToString(callSuper = true)
@RequiredArgsConstructor
@Indexed(index = "blog_comment")
@EqualsAndHashCode(callSuper = true)
public class Comment extends AbstractEntity {

    @NotNull
    @Column(name = "body", nullable = false)
    @FullTextField(analyzer = "body_analyzer")
    private String body;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "owner_id")
    @ToString.Exclude
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    private User owner;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "post_id")
    @ToString.Exclude
    @IndexingDependency(reindexOnUpdate = ReindexOnUpdate.SHALLOW)
    private BlogPost post;

}