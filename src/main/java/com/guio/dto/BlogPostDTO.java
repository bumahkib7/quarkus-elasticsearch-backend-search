package com.guio.dto;

public record BlogPostDTO(
    Long id,
    String title,
    String body,
    String description,
    String slug,
    String status,
    FeedbackmessageDTO feedback
) {}

