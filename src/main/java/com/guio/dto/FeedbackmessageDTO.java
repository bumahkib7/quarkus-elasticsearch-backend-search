package com.guio.dto;

public record FeedbackmessageDTO(
    Long id,
    String message
) {}
